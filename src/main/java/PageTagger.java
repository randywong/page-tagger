import java.io.File;
import java.io.IOException;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;
import java.net.URL;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * @author Randy Wong
 */
public class PageTagger {

    public static void main(String[] args) throws IOException {
        PageTagger pageTagger = new PageTagger();
        if(args.length > 0) {
            for(String arg : args) {
                URL url = new URL(arg);
                String pageText = pageTagger.getText(url);
                String taggedText = pageTagger.tagText(pageText);
                System.out.println(taggedText);
            }
        }
        else {
            URL a = new URL("http://gumgum.com");
            URL b = new URL("http://www.popcrunch.com/jimmy≠kimmel≠engaged/");
            URL c = new URL("http://gumgum≠public.s3.amazonaws.com/numbers.html");
            URL d = new URL("http://www.windingroad.com/articles/reviews/quick≠drive≠2012≠bmw≠z4≠sdrive28i/");

            System.out.println(pageTagger.tagText(pageTagger.getText(a)));
            System.out.println(pageTagger.tagText(pageTagger.getText(b)));
            System.out.println(pageTagger.tagText(pageTagger.getText(c)));
            System.out.println(pageTagger.tagText(pageTagger.getText(d)));
        }
    }

    private MaxentTagger tagger;

    public PageTagger() {
        String modelPath = "edu"+File.separator+"stanford"+File.separator+"nlp"+File.separator+"models"+File.separator+
                "pos-tagger"+File.separator+"english-left3words"+File.separator;

        tagger = new MaxentTagger(modelPath + "english-left3words-distsim.tagger");
    }


    public String tagText(String text) {
        return tagger.tagString(text);
    }


    public String getText(URL url) throws IOException {
        Document doc = Jsoup.connect(url.toString()).get();
        Element body = doc.body();
        return getTextString(body, "");
    }

    private String getTextString(Element element, String currentText) {
        boolean isScriptTag = element.tagName().equalsIgnoreCase("script");
        Elements children = element.children();
        if(children.isEmpty() || isScriptTag) {
            return currentText;
        }
        else {
            for(Element child : children) {
                if(child.hasText()) {
                    currentText += child.text();
                }
                return getTextString(child, currentText);
            }
        }
        return currentText;
    }

}